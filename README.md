# Artivatic Task API #

Pre-requisites -
  - Python 3.7

To run the server, follow the following steps -

 - Create Anaconda Virtual Environment & activate
 - Clone the repository
 - Install the dependencies
 - Run the django app

### Create Virtual environment

```bash
python -m venv artivatic-demo
```

To activate the virtual environment, execute the `activate.bat` file in `artivatic-demo/Scripts/`

Run,

```bash
artivatic-demo/Scripts/activate
```
This would activate the virtual environment.

### Clone the repository

To clone the repository,
```bash
git clone https://tanucs0043@bitbucket.org/tanucs0043/artivatic-demo.git
```
Note - Currently, code changes are pushed to `dev` branch. Pull accordingly. 

### Install dependencies

Once the virtual env is activated, install the required dependencies from
```bash
pip install -r requirements.txt
```

To install yagmail, run the following command,
```bash
pip install yagmail
```

Run the following command,

```bash
import yagmail
yagmail.register('your_gmailusername', 'your_gmailpassword')
```

### Usage

To run the server, make sure you are in the `ml_platform` directory and execute

```bash
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```